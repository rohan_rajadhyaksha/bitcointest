package com.example.blockchaintest.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.blockchaintest.R
import com.example.blockchaintest.models.UnConfirmedTranscationDisplayModel
import com.example.blockchaintest.view_holders.UnconfirmedViewHolder

class UnConfirmedTransactionAdapter(var transactionList:ArrayList<UnConfirmedTranscationDisplayModel>) :RecyclerView.Adapter<UnconfirmedViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UnconfirmedViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_unconfirmed_transactions, parent, false)
        return UnconfirmedViewHolder(view)
    }

    override fun getItemCount(): Int =transactionList.size

    override fun onBindViewHolder(holder: UnconfirmedViewHolder, position: Int) {
        holder.bind(transactionList[position])
    }

    fun updateTransactionList(list:ArrayList<UnConfirmedTranscationDisplayModel>){
        transactionList = list
        notifyDataSetChanged()
    }

}