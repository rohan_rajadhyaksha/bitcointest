package com.example.blockchaintest.Managers

import android.os.Handler
import android.util.Log
import androidx.lifecycle.MediatorLiveData
import com.example.blockchaintest.constants.AppConstants
import com.example.blockchaintest.controllers.WebsocketController
import com.example.blockchaintest.models.NewBlockDisplayModel
import com.example.blockchaintest.models.NewBlockTransaction
import com.example.blockchaintest.models.UnConfirmedTranscation
import com.example.blockchaintest.models.UnConfirmedTranscationDisplayModel
import com.example.blockchaintest.utils.AppUtils
import com.example.blockchaintest.utils.FixedSizeQueue
import com.google.gson.Gson

object TransactionManager {
    val gson: Gson
    val handler: Handler

    init {
        gson = Gson()
        handler = Handler()
        WebsocketController.startWebSocket()

    }

    fun getValidUnConfirmedTransactions(): MediatorLiveData<FixedSizeQueue<UnConfirmedTranscationDisplayModel>> {
        val validTransactions =MediatorLiveData<FixedSizeQueue<UnConfirmedTranscationDisplayModel>>()
        val allTransactionsData = WebsocketController.unconfirmedTransactiondata
        val fixedSizeQueue =
            FixedSizeQueue<UnConfirmedTranscationDisplayModel>(AppConstants.UNCONFIRMD_TRANSACTION_THRESHOLD)

        validTransactions.addSource(allTransactionsData) { response ->

            val unConfirmedTransaction = gson.fromJson(response, UnConfirmedTranscation::class.java)
            val satoshiValue = getSummationOfSatoshiValue(unConfirmedTransaction)
            if (isTransactionAboveThreshold(satoshiValue)) {
                val date = AppUtils.convertTimestampToDate(unConfirmedTransaction.x.time)
                var displayModel = UnConfirmedTranscationDisplayModel(
                    unConfirmedTransaction.x.hash,
                    satoshiValue,
                    date
                )
                fixedSizeQueue.add(displayModel)
                validTransactions.postValue(fixedSizeQueue)

            }

        }

        return validTransactions
    }

    fun isTransactionAboveThreshold(satoshiValue: Double): Boolean {

        return AppUtils.convertSatoshiValueToUsd(satoshiValue) > AppConstants.THRESHOLD_USD
    }

    fun getSummationOfSatoshiValue(transactionModel: UnConfirmedTranscation): Double {
        return transactionModel.x.inputs.flatMap { input -> mutableListOf(input.prev_out) }
            .sumByDouble { prevOut -> prevOut.value }
    }

    fun getNewBlock():MediatorLiveData<NewBlockDisplayModel> {
        val validTransactions =MediatorLiveData<NewBlockDisplayModel>()
        val allTransactionsData = WebsocketController.newBlockTransactionData
        validTransactions.addSource(allTransactionsData){
            val newBlockTransaction = gson.fromJson(it, NewBlockTransaction::class.java)
            var displayModel = NewBlockDisplayModel(newBlockTransaction.x.hash,newBlockTransaction.x.height,newBlockTransaction.x.totalBTCSent,newBlockTransaction.x.reward)
            validTransactions.postValue(displayModel)
        }

       return validTransactions

    }

    fun unsubscribe() {
        WebsocketController.unsubscribeSocket()
    }

}