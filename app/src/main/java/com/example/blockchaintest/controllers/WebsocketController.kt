package com.example.blockchaintest.controllers

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.blockchaintest.Managers.TransactionManager
import com.example.blockchaintest.constants.AppConstants
import com.example.blockchaintest.constants.UrlConstants
import com.example.blockchaintest.models.TransactionPojo
import com.google.gson.Gson
import okhttp3.*
import okio.ByteString


object WebsocketController {

    val okhttpClient: OkHttpClient
    val transactionRequest: Request
    var webSocket: WebSocket? = null
    var webSocketForBlock: WebSocket? = null
    val newBlockTransactionData = MutableLiveData<String>()
    val unconfirmedTransactiondata = MutableLiveData<String>()
    val gson: Gson


    init {
        okhttpClient = OkHttpClient()
        transactionRequest = Request.Builder().url(UrlConstants.SCOKET_URL).build()
        gson = Gson()
    }

    var listener = object : WebSocketListener() {
        override fun onOpen(webSocket: WebSocket, response: Response) {
            val pingJson = "{\"op\":\"ping\"}"
            val subJson = "{\"op\":\"unconfirmed_sub\"}"
            val newBlockJson = "{\"op\":\"blocks_sub\"}"
            webSocket.send(subJson)
            webSocket.send(newBlockJson)

        }

        override fun onFailure(webSocket: WebSocket, t: Throwable, response: Response?) {
        }

        override fun onMessage(webSocket: WebSocket, data: String) {
            val transaction = TransactionManager.gson.fromJson(data, TransactionPojo::class.java)

            if (transaction.op.equals(AppConstants.UNCONFIRMED_TOKEN)) {
                unconfirmedTransactiondata.postValue(data)
            } else if (transaction.op.equals(AppConstants.NEW_BLOCK_TOKEN)) {
                newBlockTransactionData.postValue(data)
            }

        }

        override fun onClosed(webSocket: WebSocket, code: Int, reason: String) {
            Log.d("Alpha", "Socket Closed")
        }

        override fun onClosing(webSocket: WebSocket, code: Int, reason: String) {

        }

        override fun onMessage(webSocket: WebSocket, bytes: ByteString) {

        }
    }


     fun startWebSocket() {
        webSocket = okhttpClient.newWebSocket(transactionRequest, listener)
        okhttpClient.dispatcher().executorService().shutdown()


    }

    fun unsubscribeSocket() {
        webSocket?.close(1000, "DONE")

    }
}