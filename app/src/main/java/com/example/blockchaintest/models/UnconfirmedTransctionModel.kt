package com.example.blockchaintest.models

data class UnConfirmedTranscation(
    val op: String,
    val x: X
)

data class X(
    val hash: String,
    val inputs: List<Input>,
    val lock_time: Int,
    val `out`: List<Out>,
    val relayed_by: String,
    val size: Int,
    val time: Long,
    val tx_index: Int,
    val ver: Long,
    val vin_sz: Long,
    val vout_sz: Long
)

data class Input(
    val prev_out: PrevOut,
    val script: String,
    val sequence: Long
)

data class PrevOut(
    val addr: String,
    val n: Int,
    val script: String,
    val spent: Boolean,
    val tx_index: Int,
    val type: Int,
    val value: Double
)

data class Out(
    val addr: String,
    val n: Int,
    val script: String,
    val spent: Boolean,
    val tx_index: Long,
    val type: Int,
    val value: Long
)