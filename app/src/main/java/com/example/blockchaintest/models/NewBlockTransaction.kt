package com.example.blockchaintest.models

data class NewBlockTransaction(
    val op: String,
    val x: NewBlockX
)

data class NewBlockX(
    val bits: Int,
    val blockIndex: Int,
    val estimatedBTCSent: Long,
    val hash: String,
    val height: Int,
    val mrklRoot: String,
    val nTx: Int,
    val nonce: Int,
    val prevBlockIndex: Int,
    val reward: Long,
    val size: Long,
    val time: Long,
    val totalBTCSent: Long,
    val txIndexes: List<Int>,
    val version: Int
)