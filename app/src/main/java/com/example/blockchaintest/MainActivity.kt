package com.example.blockchaintest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.blockchaintest.adapters.UnConfirmedTransactionAdapter
import com.example.blockchaintest.models.NewBlockDisplayModel
import com.example.blockchaintest.models.UnConfirmedTranscationDisplayModel
import com.example.blockchaintest.view_models.DashbordViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.view_new_block.*

class MainActivity : AppCompatActivity() {

    var dashboardViewModel :DashbordViewModel? =null
    var mAdapter:UnConfirmedTransactionAdapter?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init();
    }

    fun init(){
        dashboardViewModel = ViewModelProviders.of(this).get(DashbordViewModel::class.java)
        mAdapter = UnConfirmedTransactionAdapter(ArrayList())
        rvUnConfirmed.layoutManager =LinearLayoutManager(this)
        rvUnConfirmed.adapter=mAdapter
    }

    fun subscribeChannels(){
        subscribeToUnConfirmedTransactionChannel()
        subscribeToNewBlock()
    }

    private fun subscribeToUnConfirmedTransactionChannel() {
        var unCompleteTransactions = dashboardViewModel?.subsribeToUnConfirmedTransactions()
        unCompleteTransactions?.observe(this, Observer {
            val transactionList =ArrayList<UnConfirmedTranscationDisplayModel>(it)
            mAdapter?.updateTransactionList(transactionList)
        })
    }

    private fun subscribeToNewBlock(){
        var newTransaction = dashboardViewModel?.subscribeToNewBlock()
        newTransaction?.observe(this, Observer {
            intNewBlockData(it)
        })
    }

    private fun intNewBlockData(displayeModel: NewBlockDisplayModel){
        tvHash.text =displayeModel.hash
        tvBtc.text=displayeModel.btc.toString()
        tvReward.text=displayeModel.reward.toString()
        tvHeight.text=displayeModel.height.toString()
    }

    override fun onPause() {
        super.onPause()
        dashboardViewModel?.unsubscribeChannels()
    }

    override fun onResume() {
        super.onResume()
        subscribeChannels()
    }

}