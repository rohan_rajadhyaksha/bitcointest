package com.example.blockchaintest.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.blockchaintest.Managers.TransactionManager
import com.example.blockchaintest.models.NewBlockDisplayModel
import com.example.blockchaintest.models.UnConfirmedTranscationDisplayModel
import com.example.blockchaintest.utils.FixedSizeQueue

class DashbordViewModel :ViewModel() {

    fun subsribeToUnConfirmedTransactions(): LiveData<FixedSizeQueue<UnConfirmedTranscationDisplayModel>> {
        return TransactionManager.getValidUnConfirmedTransactions()
    }

    fun subscribeToNewBlock():LiveData<NewBlockDisplayModel>{
        return TransactionManager.getNewBlock()
    }


    fun unsubscribeChannels(){
        TransactionManager.unsubscribe()
    }
}