package com.example.blockchaintest.constants

object AppConstants {

    val RESULTS_DELAY: Long=5000
    const val THRESHOLD_USD=100
    const val UNCONFIRMD_TRANSACTION_THRESHOLD=5
    const val UNCONFIRMED_TOKEN="utx"
    const val NEW_BLOCK_TOKEN="block"
}