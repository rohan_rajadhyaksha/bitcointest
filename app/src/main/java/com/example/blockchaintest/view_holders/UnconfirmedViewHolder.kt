package com.example.blockchaintest.view_holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.blockchaintest.models.UnConfirmedTranscationDisplayModel
import kotlinx.android.synthetic.main.row_unconfirmed_transactions.view.*

class UnconfirmedViewHolder(val view : View): RecyclerView.ViewHolder(view) {

      fun bind(transaction:UnConfirmedTranscationDisplayModel){
          view.tvHash.text = transaction.hash
          view.tvValue.text=transaction.value.toString()
          view.tvDate.text =transaction.date

      }
}