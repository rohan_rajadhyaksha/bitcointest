package com.example.blockchaintest.utils

import java.text.SimpleDateFormat
import java.util.*

object AppUtils {

    const val SATOSHI_EQUI_USD= 13856

    /*this method assumes that one 1USD=13856 satoshi*/
    fun convertSatoshiValueToUsd(satoshiValue:Double):Double{
        return if(satoshiValue<0){
            0.0
        }else{
            satoshiValue/ SATOSHI_EQUI_USD
        }
    }

    fun convertTimestampToDate(timeStamp :Long):String{
        try {
            val sdf = SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
            val netDate = Date(timeStamp*1000L)
            sdf.timeZone = TimeZone.getDefault()
            return sdf.format(netDate)
        } catch (ex: Exception) {
            return ""
        }

    }
}