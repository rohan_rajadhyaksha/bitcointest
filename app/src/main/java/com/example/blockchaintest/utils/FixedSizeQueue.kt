package com.example.blockchaintest.utils

import android.os.Build
import androidx.annotation.RequiresApi
import java.util.concurrent.ConcurrentLinkedDeque


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class FixedSizeQueue<T> : ConcurrentLinkedDeque<T> {

    private var sizeLimit = Integer.MAX_VALUE

    constructor(c: Collection<T>) : super(c)

    constructor(sizeLimit: Int) {
        var sizeLimit = sizeLimit
        if (sizeLimit < 0) sizeLimit = 0
        this.sizeLimit = sizeLimit
    }

    constructor(c: Collection<T>, sizeLimit: Int) : super(c) {
        var sizeLimit = sizeLimit

        if (sizeLimit < 0) sizeLimit = 0
        this.sizeLimit = sizeLimit
    }

    fun getSizeLimit(): Int {
        return sizeLimit
    }

    fun setSizeLimit(sizeLimit: Int) {
        this.sizeLimit = sizeLimit
    }

    override fun addFirst(e: T) {
        while (size >= this.sizeLimit) {
            pollLast()
        }
        super.addFirst(e)
    }

    override fun addLast(e: T) {
        while (size >= this.sizeLimit) {
            pollFirst()
        }
        super.addLast(e)
    }

    override fun add(element: T): Boolean {
        if(size >= this.sizeLimit){
            pollFirst()
        }
        return super.add(element)
    }
}